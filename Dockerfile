FROM ubuntu:latest
MAINTAINER Zach Russell "zach@protechig.com"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential sqlite3
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
RUN sqlite3 /tmp/flaskr.db < schema.sql
RUN flask initdb
ENTRYPOINT ["flask"]
CMD ["run --host=0.0.0.0"]

